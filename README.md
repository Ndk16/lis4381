> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 - Mobile Web Application Development

## Nicholas Koester

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB resource links

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create website with personal information
    - Basic client-side verification
    - Skillsets 10-12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Basic server side verification
    - Prepared statements
    - Skillsets 13-15

6. [P1 README.md](p1/README.md "My p1 README.md file")
    - Create Business Card Android app
    - Provide screenshots of completed app
    - Skillsets 7 - 9

7. [P2 README.md](p2/README.md "My p2 README.md file")
    - Basic server side validation
    - PHP server-side validation
    - Addition of edit and delete functions to a database

