> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Nicholas Koester

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions

> #### Git commands w/short descriptions:

1. git init - Creates an empty Git repository
2. git status - Shows the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of AMPPS running:

![AMPPS Screenshot](img/ampps_run.png "AMPPS running")

*Screenshot of running java hello:

![java hello Screenshot](img/java_hello.png "Java hello running")

*Screenshot of Android Studio 1/2 - My First App:

![Android studio](img/first_app1.png "My First App running in Android Studio")

*Screenshot of Android Studio 2/2 - My First App:

![Android studio](img/first_app2.png "My First App running in Android Studio")





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


