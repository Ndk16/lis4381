
# LIS4381

## Nicholas Koester

### Assignment 2 Requirements:

*Two Parts:*

1. Screenshot of running application's first user interface;
2. Screenshot of running applcation's second user interface;


#### Assignment Screenshots:

*Screenshot of screen 1:

![Healthy Recipes home screen](img/homeScreen.png "Home screen")

*Screenshot of screen 2:

![Bruschetta Recipe](img/recipeScreen.png "Recipe screen")




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


