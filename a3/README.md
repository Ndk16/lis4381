
# LIS4381

## Nicholas Koester

### Assignment 3 Requirements:

*Three Parts:*

1. Screenshot of ERD
2. Screenshot of running application's first user interface;
3. Screenshot of running applcation's second user interface;


#### Assignment Screenshots:

*Screenshot of ERD:

![ERD](img/ERD.png "Completed ERD")

*Screenshot of screen 1:

![My Event home screen](img/firstScreen.png "Home screen")

*Screenshot of screen 2:

![Ticket price](img/secondScreen.png "Tickets ordered")

*Skillset 4 1/3:

![S4 1/3](img/ss4p1.png "Skillset 4 running")

*Skillset 4 2/3:

![S4 2/3](img/ss4p2.png "Skillset 4 running")

*Skillset 4 3/3:

![S4 3/3](img/ss4p3.png "Skillset 4 running")

*Skillset 5:

![S5](img/ss5.png "Skillset 5 running")

*Skillset 6:

![S6](img/ss6.png "Skillset 6 running")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


