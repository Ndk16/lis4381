<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Nicholas Koester">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <style>
  	.navbar-inverse
		{
    		background-image: linear-gradient(to bottom, #782f40, #000000 200%);
		}
	</style>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong> Frequently, not only will you be asked to design and develop Web applications, 
					but you will also be asked to create (design) database solutions that interact with the client/server (e.g., web)
					application--and, in factm the data repository is the *core* of all Web applications. Hence, the following business requirements.
				</p>
				<p class="text-justify">
					A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track,
					and maintain relevant company data, based upon the following business rules:
				</p>


				<h4>Java Installation</h4>
				<img src="img/java_hello.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio Installation</h4>
				<img src="img/first_app1.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>AMPPS Installation</h4>
				<img src="img/amps_runn.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
