
# LIS4381

## Nicholas Koester

### Assignment 4 Requirements:

*Three Parts:*

1. Screenshot of localhost lis4381 home webpage;
2. Basic client-side validation;
3. Skillsets 10-12


#### Assignment Screenshots:

*Screenshot of Localhost home screen:

![home Screen](img/home.png "Localhost home screen")

*Screenshot of Failed Validation:

![Failed Validation](img/a4_1.png "Failed Validation")

*Screenshot of Passed Validation:

![Passed Validation](img/a4_verified.png "Passed Validation")

*Skillset 10 1/2:

![S10 1/2](img/ss10_1.png "Skillset 10 running")

*Skillset 10 2/2:

![S10 2/2](img/ss10_2.png "Skillset 10 running")

*Skillset 11 1/4:

![S11 1/4](img/ss11_1.png "Skillset 11 running")

*Skillset 11 2/4:

![S11 2/4](img/ss11_2.png "Skillset 11 running")

*Skillset 11 3/4:

![S11 3/4](img/ss11_3.png "Skillset 11 running")

*Skillset 11 4/4:

![S11 4/4](img/ss11_4.png "Skillset 11 running")


*Skillset 12:

![S6](img/ss12.png "Skillset 6 running")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


