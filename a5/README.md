
# LIS4381

## Nicholas Koester

### Assignment 5 Requirements:

*Two Parts:*

1. Basic server-side validation
2. Skillsets 13-15


#### Assignment Screenshots:

*Screenshot of index.php:

![index Screen](img/index.png "Index screen")

*Screenshot of Failed Validation:

![Failed Validation](img/invalid.png "Failed Validation")

*Screenshot of Passed Validation:

![Passed Validation](img/valid.png "Passed Validation")

*Screenshot of added_petstore_process(passed_validation):

![Added item](img/added_item.png "Added item to petstore")

*Skillset 13:

![S13](img/ss13.png "Skillset 13 running")

*Skillset 14:

![S14](img/ss14.png "Skillset 14 running")


*Skillset 15:

![S15](img/ss15.png "Skillset 15 running")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


