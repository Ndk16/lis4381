
# LIS4381

## Nicholas Koester

### Project 1 Requirements:

*Two Parts:*

1. Screenshot of running application's first user interface;
2. Screenshot of running applcation's second user interface;


#### Assignment Screenshots:

*Screenshot of screen 1:

![Business card home screen](img/BC_1.png "Home screen")

*Screenshot of screen 2:

![Contact info](img/BC_2.png "Contact information screen")

*Skillset 7 1/2:

![S7 1/2](img/ss7_1.png "Skillset 7 running")

*Skillset 7 2/2:

![S7 2/2](img/ss7_2.png "Skillset 7 running")

*Skillset 8 1/2:

![S8 1/2](img/ss8_1.png "Skillset 8 running")

*Skillset 8 2/2:

![S8 2/2](img/ss8_2.png "Skillset 8 running")


*Skillset 9 1/2:

![S9 1/2](img/ss9_1.png "Skillset 9 running")

*Skillset 9 2/2:

![S9 2/2](img/ss9_2.png "Skillset 9 running")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


