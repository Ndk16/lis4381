
# LIS4381

## Nicholas Koester

### Project 2 Requirements:

*Two Parts:*

1. Basic server-side validation
2. RSS Feed


#### Assignment Screenshots:

*Screenshot of Home page:

![home Screen](img/home.png "Home screen")

*Screenshot of index.php:

![index Screen](img/index.png "Index screen")

*Screenshot of Failed Validation:

![Failed Validation](img/error.png "Failed Validation")

*Screenshot of Passed Validation:

![Passed Validation](img/passed.png "Passed Validation")

*Screenshot of Delete record prompt:

![delete record prompt](img/delete.png "Delete record prompt")

*Screenshot of Successfully Deleted Record:

![deleted record](img/deleted_record.png "Record deleted")

*Screenshot of RSS feed page:

![RSS Feed of Nasa education](img/rss.png "Rss feed")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


