//NAME: Nicholas Koester
// 9-23-2021
// LIS 4381 - Skillset 3



public class ArraysAndLoops {
    public static void main(String[] args) {
        System.out.println("Program loops through array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");

        String animals[] = {"dog", "cat", "bird", "fish", "insect"};

        System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
        System.out.println("For loop:");

        for(int i = 0; i < animals.length; i++){
            System.out.println(animals[i]);
        }

        System.out.println("\nEnhanced for Loop:");
        for(String test : animals){
            System.out.println(test);
        }

        System.out.println("\nwhile loop");
        int i = 0;
        while (i < animals.length){
            System.out.println(animals[i]);
            i++;
        }

        i = 0;
        System.out.println("\ndo...while loop: ");
        do {
            System.out.println(animals[i]);
            i++;
        } while(i < animals.length);

    }
}
