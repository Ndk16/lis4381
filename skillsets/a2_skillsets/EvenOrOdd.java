//NAME: Nicholas Koester
// 9-23-2021
// LIS 4381 - Skillset 1

import java.util.Scanner;

public class EvenOrOdd {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("program evaluates intergers as even or odd.");
        System.out.println("Note: Program does *not* check for non-numeric characters.");
        System.out.print("Enter integer: ");
        int userInput = 0;
        userInput = input.nextInt();

        if(userInput % 2 == 0){
            System.out.println(userInput + " is an even number.");
        }
        else {
            System.out.println(userInput + " is an odd number.");
        }

    }
}
