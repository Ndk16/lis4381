//NAME: Nicholas Koester
// 9-23-2021
// LIS 4381 - Skillset 2

import java.util.Scanner;

public class LargestNumber {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Program evaluates largest of two integers");
        System.out.println("Note: Program does *not* check for non-numeric values.");
        System.out.print("Enter first integer: ");
        int firstInt = 0;
        firstInt = input.nextInt();

        System.out.print("Enter second integer: ");
        int secondInt = 0;
        secondInt = input.nextInt();

        if(firstInt > secondInt){
            System.out.println(firstInt + " is larger than " + secondInt);
        }
        else if(firstInt < secondInt){
            System.out.println(secondInt + " is larger than " + firstInt);
        }
        else if( firstInt == secondInt){
            System.out.println("Integers are equal.");
        }

    }
}
