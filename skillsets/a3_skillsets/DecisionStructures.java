//NAME: Nicholas Koester
// 10-7-2021
// LIS 4381 - Skillset 4

import java.util.Scanner;

import static java.lang.Character.toUpperCase;

public class DecisionStructures {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use following characters: W or w, C or c, H or h, N or n.");
        System.out.println("Use following decision structures: if...else, and switch");
        System.out.println();
        System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none),");
        System.out.print("Enter phone type: ");

        char phone_type = input.next().charAt(0);
        phone_type = toUpperCase(phone_type);

        System.out.println("if...else:");
        if(phone_type == 'W'){
            System.out.println("Phone type: work");
        }
        else if(phone_type == 'C'){
            System.out.println("Phone type: cell");
        }
        else if(phone_type == 'H'){
            System.out.println("Phone type: home");
        }
        else if(phone_type == 'N'){
            System.out.println("Phone type: none");
        }
        else if(phone_type != 'W' || phone_type != 'C' || phone_type != 'H' || phone_type != 'N'){
            System.out.println("Incorrect character entry.");
        }

        System.out.println("switch:");
        switch (phone_type) {
            case 'W':
                System.out.println("Phone type: work");
                break;
            case 'C':
                System.out.println("Phone type: cell");
                break;
            case 'H':
                System.out.println("Phone type: home");
                break;
            case 'N':
                System.out.println("Phone type: none");
                break;
            default:
                System.out.println("Incorrect character entry.");
                break;
        }

    }
}
