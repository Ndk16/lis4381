//NAME: Nicholas Koester
// 10-7-2021
// LIS 4381 - Skillset 6

import java.util.Scanner;
import java.util.*;

public class Methods {
    public static void getRequirement() {
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("Create four methods from the following requirements:");
        System.out.println("1) getRequirements(): Void method displays program requirements.");
        System.out.println("2) getUserInput(): Void method displays program requirements.");
        System.out.println("3) myVoidMethod():\n" +
                            "\ta. Accepts two arguments: String and int. \n" +
                            "\tb. Prints user's first name and age.");
        System.out.println("4) myValueReturningMethod():\n" +
                            "\ta. Accepts two arguments: String and int. \n" +
                            "\tb. Returns Strong containing first name and age.");

    }

    public static void getUserInput() {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter first name: ");
        String name = input.next();

        System.out.print("\nEnter age: ");
        int age = input.nextInt();

        myVoidMethod(name,age);
        System.out.println(myValueReturningMethod(name, age));
    };

    public static void myVoidMethod(String name, int age) {
        System.out.println("\nvoid method call: " + name + " is " + age);
    };

    public static String myValueReturningMethod(String name, int age) {
        String methodReturn = "value returning method call: " + name + " is " + age;

        return methodReturn;
    };

    public static void main(String[] args) {
        System.out.println("Program prompts user for first name and age. then print results.");
        System.out.println("Create four methods from the following requirements.");
        Methods.getRequirement();
        Methods.getUserInput();
    }
};
