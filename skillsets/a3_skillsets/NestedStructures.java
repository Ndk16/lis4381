//NAME: Nicholas Koester
// 10-7-2021
// LIS 4381 - Skillset 5

import java.util.Scanner;

public class NestedStructures {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Program searches user-entered integer w/in array of integers.");
        System.out.println("Create an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7");

        int[] test = {3, 2, 4, 99, -1, -5, 3, 7};
        System.out.println("\nArray Length: " + test.length);

        System.out.println("Enter search value: ");
        int val = input.nextInt();

        for(int i = 0; i < test.length; i++){
            if (test[i] == val){
                System.out.println(val + " is found at index " + i);
            }
            else
            {
                System.out.println(val + " is *not* found at index" + i);
            }
        }

    }
}
